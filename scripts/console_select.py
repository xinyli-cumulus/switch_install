#!/usr/bin/env python
import pexpect
import time
import sys

# the script takes an argument for console connection to the switch
# e.g "ssh 10.90.2.2 -p 7027"

def get_menu_selections(conn):
    KEY_DOWN = '\033[B'
    print ('Waiting for GNU GRUB to show')
    conn.expect_exact("GNU GRUB", timeout=480)
    time.sleep(1)
    conn.expect_exact('Use the ^ and v keys')
    time.sleep(1)
    conn.expect_exact('ONIE')
    conn.send(KEY_DOWN)
    conn.send(KEY_DOWN)
    conn.send(KEY_DOWN)
    #conn.sendcontrol('m')
    time.sleep(10)
    conn.sendline('')
    time.sleep(10)
    conn.sendline('YES')
    time.sleep(10)
    conn.sendline('')


def main():
    print(sys.argv[1])
    connection = pexpect.spawn(str(sys.argv[1]))
   # reboot box
    get_menu_selections(connection)

main()
